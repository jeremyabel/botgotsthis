class NoPingException(Exception):
    pass

class LoginUnsuccessfulException(Exception):
    pass
