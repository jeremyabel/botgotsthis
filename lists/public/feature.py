﻿features = {
    'textconvert': 'Text Character Conversion',
    'modpyramid': 'Mods Using !pyramid',
    'modwall': 'Mods Using !wall',
    'nocustom': 'Disable Custom Commands',
    'nourlredirect': 'Ban URL Redirect (user has no follows)',
    'gamestatusbroadcaster': '!game and !status only for broadcaster',
    }
